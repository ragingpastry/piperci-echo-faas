from flask import Request
from shutil import which
from piperci.faas.exceptions import PiperDelegateError, PiperError
from piperci.faas.this_task import ThisTask


class ValidationError(Exception):
    pass


def handle(request: Request, task: ThisTask, config: dict):
    """
    Entrypoint to the echo FaaS.
    :param request: The request object from Flask
    :param task: The ThisTask instance for this class required for this faas
    :param config:
    :return: (response_body, code)
    """
    try:
        task.info("Echo faas gateway handler.handle called successfully")
    except PiperError as e:
        return {"error": f"{str(e)}: no delegate was attempted"}, 400

    try:
        task.delegate(config["executor_url"], task.task)
        return task.complete("Echo gateway completed successfully")
    except PiperDelegateError as e:
        return {"error": f"{str(e)}"}, 400


def validate(request: Request, config: dict, **kwargs):
    if "task" in kwargs:
        task = kwargs["task"]
    else:
        task = None

    try:
        validate_commands_installed(request=request, config=config, task=task)
    except AssertionError:
        raise ValidationError


def validate_commands_installed(request: Request, config: dict, task: ThisTask = None):
    request_json = request.get_json()
    request_command_param = request_json["run-cmd"]
    supported_commands = config["supported_commands"]

    if isinstance(request_command_param, str):
        request_program = request_command_param.split()[0]
        assert which(request_program) is not None
        assert request_program in supported_commands
    elif isinstance(request_command_param, list):
        request_programs = list(map(lambda x: x.split()[0], request_command_param))
        for request_program in request_programs:
            assert which(request_program) is not None
            assert request_program in supported_commands
    else:
        if task:
            task.info(
                f"run-cmd in request json did not match expected type and "
                + f"instead is type {type(request_command_param)}"
            )
        raise TypeError
