import pytest
import responses

from piperci_echo_gateway.function.faas_app import app as papp
from piperci_echo_gateway.function.config import Config
from typing import Tuple


@pytest.fixture
def app():  # required by pytest_flask
    return papp


@pytest.fixture
def config():
    return Config


@pytest.fixture
def task_post_executor_url_response(config) -> Tuple[Tuple, dict]:
    return (
        (responses.POST, config["executor_url"].format(endpoint=config["endpoint"])),
        dict(),
    )


@pytest.fixture(scope="function")
def valid_echo_request(single_instance):
    echo_vars = single_instance["run-cmd"][1]
    return single_instance, echo_vars


@pytest.fixture(scope="function")
def invalid_command_request(single_instance):
    invalid_command = single_instance
    invalid_command["run-cmd"] = ["broken_cmd", "arg"]

    return invalid_command, None
