## [1.0.1](https://gitlab.com/dreamer-labs/piperci/piperci-echo-faas/compare/v1.0.0...v1.0.1) (2019-09-09)


### Bug Fixes

* Adjust tests and remove validations calls ([9b1b55f](https://gitlab.com/dreamer-labs/piperci/piperci-echo-faas/commit/9b1b55f))

# 1.0.0 (2019-08-14)


### Features

* echo_faas executes passed run-cmd ([0e5cf9a](https://gitlab.com/dreamer-labs/piperci/piperci-echo-faas/commit/0e5cf9a))
* ****/handler.validate:** Add support for 1+ commands in one request ([f1caaf1](https://gitlab.com/dreamer-labs/piperci/piperci-echo-faas/commit/f1caaf1))
